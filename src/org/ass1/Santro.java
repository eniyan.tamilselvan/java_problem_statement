package org.ass1;

public class Santro extends Car implements BasicCar {
			
	private void remotestart() {
		System.out.println("start by remote");
	}

	@Override
	public void gearchange() {
		System.out.println("gear will change");		
	}

	@Override
	public void music() {
		System.out.println("play the music");		
	}

	public static void main(String[] args) {
		Santro santro = new Santro();
		santro.remotestart();
		santro.gearchange();
		santro.music();
		santro.drive();
		santro.stop();
		
	}
}
