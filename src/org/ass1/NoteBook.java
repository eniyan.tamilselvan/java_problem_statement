package org.ass1;

public class NoteBook extends Book{

	public void Draw() {
	System.out.println("draw the book");	
	}

	@Override
	public void Write() {
	System.out.println("Write a book");			
	}

	@Override
	public void Read() {
	System.out.println("Read a book");			
	}

	public static void main(String[] args) {
		NoteBook book = new NoteBook();
		book.Draw();
		book.Write();
		book.Read();
	}
}

