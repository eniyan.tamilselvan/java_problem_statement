package org.ass2d;

public class AreaBase extends Area {

	public static void main(String[] args) {
		AreaBase ab = new AreaBase();
		ab.squareArea(7);
		ab.rectangleArea(11, 17);
		ab.circleArea(5.5);
	}
}

