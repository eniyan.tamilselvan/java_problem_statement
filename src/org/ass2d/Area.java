package org.ass2d;

public class Area {

	void squareArea(float x) {
		System.out.println("the area of the square is " + Math.pow(x, 2) + " sq units");
	}
	void rectangleArea(float x, float y) {
		System.out.println("the area of the rectangle is " + x * y + " sq units");
	}
	void circleArea(double x) {
		double z = 3.14 * x * x;
		System.out.println("the area of the circle is " + z + " sq units");
	}
}

