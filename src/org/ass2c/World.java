package org.ass2c;

public class World {
	
	public static void main(String[] args) {
		String str = "Hello, World";
		int indexOf = str.indexOf('o');
		System.out.println("First occurence of (o)= "+indexOf);
		
		int lastIndexOf = str.lastIndexOf('o');
		System.out.println("Last occurence of (o)= "+lastIndexOf);
		
		int indexOf2 = str.indexOf(',');
		System.out.println("First occurence of (,)= "+indexOf2);
		
		int lastIndexOf2 = str.lastIndexOf(',');
		System.out.println("Last occurence of (,)= "+lastIndexOf2);
		
	}

	
}
