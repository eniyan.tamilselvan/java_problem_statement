package org.ass2e;

public abstract class Shape {

	public abstract void rectangleArea(float x, float y);
	public abstract void  squareArea(float y);
	public abstract void circleArea(double x);
}
