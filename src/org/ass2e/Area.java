package org.ass2e;

public class Area extends Shape {

	@Override
	public void rectangleArea(float x, float y) {
		System.out.println("the area of the rectangle is " + x * y + " sq units");
		
	}
	@Override
	public void squareArea(float x) {
		System.out.println("the area of the square is " + Math.pow(x, 2) + " sq units");
	}
	@Override
	public void circleArea(double x) {
		double z = 3.14 * x * x;
		System.out.println("the area of the circle is " + z + " sq units");
	}
	public static void main(String[] args) {
		Area a = new Area();
		a.squareArea(7);
		a.rectangleArea(11, 16);
		a.circleArea(5.5);
	}
}

