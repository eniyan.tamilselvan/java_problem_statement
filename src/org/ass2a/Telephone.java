package org.ass2a;

public abstract class Telephone {

	public abstract void lift();
	public abstract void disconnected();
}
