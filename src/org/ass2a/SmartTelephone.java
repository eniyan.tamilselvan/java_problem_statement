package org.ass2a;

public class SmartTelephone extends Telephone{

	@Override
	public void disconnected() {
		System.out.println("Overrided from Telephone Class");
	}
	@Override
	public void lift() {
		System.out.println("Overrided from Telephone Class");
	}
	public static void main(String[] args) {
		SmartTelephone stp = new SmartTelephone();
		stp.disconnected();
		stp.lift();
	}
}

